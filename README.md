# [![SiaClassic Logo](http://siaclassic.tech/img/svg/siaclassic-green-logo.svg)](http://siaclassic.tech/) Nodejs Wrapper

[![Build Status](https://travis-ci.org/moderndevgroup/Nodejs-SiaClassic.svg?branch=master)](https://travis-ci.org/moderndevgroup/Nodejs-SiaClassic)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)
[![devDependency Status](https://david-dm.org/moderndevgroup/Nodejs-SiaClassic/dev-status.svg)](https://david-dm.org/moderndevgroup/Nodejs-SiaClassic#info=devDependencies)
[![dependencies Status](https://david-dm.org/moderndevgroup/Nodejs-SiaClassic.svg)](https://david-dm.org/moderndevgroup/Nodejs-SiaClassic#info=dependencies)
[![license:mit](https://img.shields.io/badge/license-mit-blue.svg)](https://opensource.org/licenses/MIT)

# A Highly Efficient Decentralized Storage Network

This is a [Nodejs](https://nodejs.org/) wrapper for
[SiaClassic](https://gitlab.com/moderndevgroup/SiaClassic). Use it in your apps to easily
interact with the SiaClassic storage network via function calls instead of manual http
requests.

## Prerequisites

- [node & npm (version 5.9.0+ recommended)](https://nodejs.org/download/)

## Installation

```
npm install siaclassic.js
```

## Example Usage

```js
import { connect } from 'siaclassic.js'

// Using promises...
// connect to an already running SiaClassic daemon on localhost:7780 and print its version
connect('localhost:7780')
  .then((siaclassicd) => {
    siaclassicd.call('/daemon/version').then((version) => console.log(version))
  })
  .catch((err) => {
    console.error(err)
  })

// Or ES7 async/await
async function getVersion() {
  try {
    const siaclassicd = await connect('localhost:7780')
    const version = await siaclassicd.call('/daemon/version')
    console.log('SiaClassicd has version: ' + version)
  } catch (e) {
    console.error(e)
  }
}

```
You can also forgo using `connect` and use `call` directly by providing an API address as the first parameter:

```js
import { call } from 'siaclassic.js'

async function getVersion(address) {
  try {
    const version = await call(address, '/daemon/version')
    return version
  } catch (e) {
    console.error('error getting ' + address + ' version: ' + e.toString())
  }
}

console.log(getVersion('10.0.0.1:7780'))
```

`siaclassic.js` can also launch a siaclassicd instance given a path on disk to the `siaclassicd` binary.  `launch` takes an object defining the flags to use as its second argument, and returns the `child_process` object.  You are responsible for keeping track of the state of this `child_process` object, and catching any errors `launch` may throw.

```js
import { launch } from 'siaclassic.js'

try {
  // Flags are passed in as an object in the second argument to `launch`.
  // if no flags are passed, the default flags will be used.
  const siaclassicdProcess = launch('/path/to/your/siaclassicd', {
    'modules': 'cghmrtw',
    'profile': true,
  })
  // siaclassicdProcess is a ChildProcess class.  See https://nodejs.org/api/child_process.html#child_process_class_childprocess for more information on what you can do with it.
  siaclassicdProcess.on('error', (err) => console.log('siaclassicd encountered an error ' + err))
} catch (e) {
  console.error('error launching siaclassicd: ' + e.toString())
}
```

The call object passed as the first argument into call() are funneled directly
into the [`request`](https://gitlab.com/request/request) library, so checkout
[their options](https://gitlab.com/request/request#requestoptions-callback) to
see how to access the full functionality of [SiaClassic's
API](https://gitlab.com/moderndevgroup/SiaClassic/blob/master/doc/API.md)

```js
SiaClassicd.call({
  url: '/consensus/block',
  method: 'GET',
  qs: {
    height: 0
  }
})
```

Should log something like:

```bash
null { block:
 { parentid: '0000000000000000000000000000000000000000000000000000000000000000',
   nonce: [ 0, 0, 0, 0, 0, 0, 0, 0 ],
   timestamp: 1433600000,
   minerpayouts: null,
   transactions: [ [Object] ] } }
```
